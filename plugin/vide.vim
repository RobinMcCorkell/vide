" Dependencies:
" Plug 'w0rp/ale'
" Plug 'Shougo/deoplete.nvim', {'do': ':UpdateRemotePlugins'}
" Plug 'autozimu/LanguageClient-neovim', {
"     \ 'branch': 'next',
"     \ 'do': 'bash install.sh',
"     \ }

if get(g:, 'vide#ale#enable', 1)
    call vide#ale#Enable()
endif

if get(g:, 'vide#lsp#enable', 1)
    call vide#lsp#Enable()
endif

let g:deoplete#enable_at_startup = 1
let g:deoplete#omni#functions = {}
let g:deoplete#omni#input_patterns = {}
