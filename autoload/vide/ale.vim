function! vide#ale#Enable() abort
    if get(g:, 'vide#ale#autofmt', 1)
        let g:ale_fix_on_save = 1
        command! -nargs=1 VideAleAutofmtBuffer let b:ale_fixers = [<q-args>]
    endif

    let g:ale_linters_explicit = 1
    let g:ale_linters = {}
    let g:ale_fixers = {}

    command! -nargs=1 VideAleLintBuffer let b:ale_linters = [<q-args>]
endfunction
